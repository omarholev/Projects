﻿namespace Övnin_3._8_Second_Try
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Bataljon", 0, 0);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tvwBataljon = new System.Windows.Forms.TreeView();
            this.btnTaBort = new System.Windows.Forms.Button();
            this.btnÅngra = new System.Windows.Forms.Button();
            this.btnRäknaSoldater = new System.Windows.Forms.Button();
            this.tbxAntalSoldater = new System.Windows.Forms.TextBox();
            this.btnNyEnhet = new System.Windows.Forms.Button();
            this.tbxNamn = new System.Windows.Forms.TextBox();
            this.tbxInfo = new System.Windows.Forms.TextBox();
            this.gbxLäggTillEnhet = new System.Windows.Forms.GroupBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.gbxLäggTillEnhet.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvwBataljon
            // 
            this.tvwBataljon.ImageIndex = 0;
            this.tvwBataljon.ImageList = this.imageList1;
            this.tvwBataljon.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.tvwBataljon.Indent = 10;
            this.tvwBataljon.Location = new System.Drawing.Point(12, 29);
            this.tvwBataljon.Name = "tvwBataljon";
            treeNode2.ImageIndex = 0;
            treeNode2.Name = "Bataljon";
            treeNode2.SelectedImageIndex = 0;
            treeNode2.Text = "Bataljon";
            this.tvwBataljon.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.tvwBataljon.SelectedImageIndex = 0;
            this.tvwBataljon.Size = new System.Drawing.Size(164, 258);
            this.tvwBataljon.TabIndex = 0;
            this.tvwBataljon.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwBataljon_AfterSelect);
            // 
            // btnTaBort
            // 
            this.btnTaBort.Location = new System.Drawing.Point(311, 29);
            this.btnTaBort.Name = "btnTaBort";
            this.btnTaBort.Size = new System.Drawing.Size(75, 23);
            this.btnTaBort.TabIndex = 1;
            this.btnTaBort.Text = "Ta Bort";
            this.btnTaBort.UseVisualStyleBackColor = true;
            this.btnTaBort.Click += new System.EventHandler(this.btnTaBort_Click);
            // 
            // btnÅngra
            // 
            this.btnÅngra.Location = new System.Drawing.Point(311, 58);
            this.btnÅngra.Name = "btnÅngra";
            this.btnÅngra.Size = new System.Drawing.Size(75, 23);
            this.btnÅngra.TabIndex = 2;
            this.btnÅngra.Text = "Ångra";
            this.btnÅngra.UseVisualStyleBackColor = true;
            this.btnÅngra.Click += new System.EventHandler(this.btnÅngra_Click);
            // 
            // btnRäknaSoldater
            // 
            this.btnRäknaSoldater.Location = new System.Drawing.Point(311, 87);
            this.btnRäknaSoldater.Name = "btnRäknaSoldater";
            this.btnRäknaSoldater.Size = new System.Drawing.Size(75, 200);
            this.btnRäknaSoldater.TabIndex = 3;
            this.btnRäknaSoldater.Text = "Räkna Antalet Soldater";
            this.btnRäknaSoldater.UseVisualStyleBackColor = true;
            this.btnRäknaSoldater.Click += new System.EventHandler(this.btnRäknaSoldater_Click);
            // 
            // tbxAntalSoldater
            // 
            this.tbxAntalSoldater.Location = new System.Drawing.Point(6, 84);
            this.tbxAntalSoldater.Name = "tbxAntalSoldater";
            this.tbxAntalSoldater.Size = new System.Drawing.Size(75, 20);
            this.tbxAntalSoldater.TabIndex = 5;
            // 
            // btnNyEnhet
            // 
            this.btnNyEnhet.Location = new System.Drawing.Point(6, 26);
            this.btnNyEnhet.Name = "btnNyEnhet";
            this.btnNyEnhet.Size = new System.Drawing.Size(75, 23);
            this.btnNyEnhet.TabIndex = 6;
            this.btnNyEnhet.Text = "Lägg Till";
            this.btnNyEnhet.UseVisualStyleBackColor = true;
            this.btnNyEnhet.Click += new System.EventHandler(this.btnNyEnhet_Click);
            // 
            // tbxNamn
            // 
            this.tbxNamn.Location = new System.Drawing.Point(6, 58);
            this.tbxNamn.Name = "tbxNamn";
            this.tbxNamn.Size = new System.Drawing.Size(75, 20);
            this.tbxNamn.TabIndex = 7;
            // 
            // tbxInfo
            // 
            this.tbxInfo.Location = new System.Drawing.Point(215, 151);
            this.tbxInfo.Multiline = true;
            this.tbxInfo.Name = "tbxInfo";
            this.tbxInfo.ReadOnly = true;
            this.tbxInfo.Size = new System.Drawing.Size(90, 136);
            this.tbxInfo.TabIndex = 9;
            // 
            // gbxLäggTillEnhet
            // 
            this.gbxLäggTillEnhet.Controls.Add(this.tbxNamn);
            this.gbxLäggTillEnhet.Controls.Add(this.tbxAntalSoldater);
            this.gbxLäggTillEnhet.Controls.Add(this.btnNyEnhet);
            this.gbxLäggTillEnhet.Location = new System.Drawing.Point(215, 29);
            this.gbxLäggTillEnhet.Name = "gbxLäggTillEnhet";
            this.gbxLäggTillEnhet.Size = new System.Drawing.Size(90, 116);
            this.gbxLäggTillEnhet.TabIndex = 10;
            this.gbxLäggTillEnhet.TabStop = false;
            this.gbxLäggTillEnhet.Text = "Lägg Till";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Bataljon.png");
            this.imageList1.Images.SetKeyName(1, "Kompani.png");
            this.imageList1.Images.SetKeyName(2, "Pluton.png");
            this.imageList1.Images.SetKeyName(3, "Grupp.png");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gbxLäggTillEnhet);
            this.Controls.Add(this.tbxInfo);
            this.Controls.Add(this.btnRäknaSoldater);
            this.Controls.Add(this.btnÅngra);
            this.Controls.Add(this.btnTaBort);
            this.Controls.Add(this.tvwBataljon);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbxLäggTillEnhet.ResumeLayout(false);
            this.gbxLäggTillEnhet.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tvwBataljon;
        private System.Windows.Forms.Button btnTaBort;
        private System.Windows.Forms.Button btnÅngra;
        private System.Windows.Forms.Button btnRäknaSoldater;
        private System.Windows.Forms.TextBox tbxAntalSoldater;
        private System.Windows.Forms.Button btnNyEnhet;
        private System.Windows.Forms.TextBox tbxNamn;
        private System.Windows.Forms.TextBox tbxInfo;
        private System.Windows.Forms.GroupBox gbxLäggTillEnhet;
        private System.Windows.Forms.ImageList imageList1;
    }
}

