﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Övnin_3._8_Second_Try
{
    public partial class Form1 : Form
    {
        const int BATALJON = 0, KOMPANI = 1, PLUTON = 2, GRUPP = 3;
        private string[] info = { "Bataljon", "Kompani", "Pluton", "Grupp" };
        Stack<TreeNode> NodeStack = new Stack<TreeNode>();
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTaBort_Click(object sender, EventArgs e)
        {
            tvwBataljon.SelectedNode.Remove();
        }

        private void tvwBataljon_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode selectedNode = tvwBataljon.SelectedNode;
            tbxInfo.Text = info[selectedNode.Level];
            tbxInfo.AppendText("\r\nNamn: " + selectedNode.Text);

            if (selectedNode.Level == GRUPP) gbxLäggTillEnhet.Enabled = false;
            else gbxLäggTillEnhet.Enabled = true;

            if (selectedNode.Level == PLUTON) tbxAntalSoldater.Enabled = true;
            else tbxAntalSoldater.Enabled = false;
        }
        private void btnÅngra_Click(object sender, EventArgs e)
        {
            if (NodeStack.Count() != 0)
            tvwBataljon.Nodes.Remove(NodeStack.Pop());
        }

        private void btnRäknaSoldater_Click(object sender, EventArgs e)
        {
            tbxInfo.AppendText("\r\nAntal Soldater: " + RäknaSoldater(tvwBataljon.SelectedNode));
        }

        int RäknaSoldater(TreeNode n)
        {
            int antal = 0;
            if (n.Level == GRUPP) { antal += (int)n.Tag;  }
            else { foreach (TreeNode m in n.Nodes) { antal += RäknaSoldater(m); } }
            return antal;
        }

        private void btnNyEnhet_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = tvwBataljon.SelectedNode;
            if (selectedNode != null)
            {
                string name;
                if (tbxNamn.Text != "")
                {
                    name = tbxNamn.Text;
                }
                else
                {
                    name = info[selectedNode.Level + 1];
                }
                TreeNode newNode = new TreeNode(name);
                selectedNode.Nodes.Add(newNode);
                if (newNode.Level == GRUPP)
                {
                    int numOfSoldiers;
                    if (int.TryParse(tbxAntalSoldater.Text, out numOfSoldiers)) newNode.Tag = numOfSoldiers;
                }
                newNode.ImageIndex = newNode.Level;
                newNode.SelectedImageIndex = newNode.Level;

                NodeStack.Push(newNode);

                selectedNode.Expand();
                tvwBataljon.SelectedNode = newNode;
            }
        }
    }
}
